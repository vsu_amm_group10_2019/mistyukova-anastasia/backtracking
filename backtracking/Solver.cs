﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace backtracking
{
    class Solver
    {
        private List<Point> Points { get; set; }
        private int Number { get; set; }
        private int Count { get; set; }
        public List<Point> List { get; set; } = new List<Point>();
        private List<Point> Solution { get; set; } = new List<Point>();

        private double min { get; set; }
        private double Distance (Point t1)
        {
            return Math.Sqrt((t1.x) * (t1.x) + (t1.y) * (t1.y) + (t1.z) * (t1.z));
        }

        private double Result(List<Point> list)
        {
            Point res = new Point();
            foreach (Point p in list)
            {
                res.x += p.x;
                res.y += p.y;
                res.z += p.z;
            }
            res.x /= Number;
            res.y /= Number;
            res.z /= Number;
            return Distance(res);
        }

        public Solver(int number, int count, List<Point> points)
        {
            Number = number;
            Count = count;
            Points = points;
        }

        public void Solve()
        {
            HashSet<int> visited = new HashSet<int>();
            for (int i = 0; i < Number; i++)
            {
                List.Add(Points[i]);
            }
            min = Result(List);
            Solution.Clear();
            Solution.AddRange(List);
            Solution.Remove(Solution.Last());
            Help(visited);
        }

        public void Help(HashSet<int> visited)
        {
            for (int i = 0; i < Count; i++)
            {
                if (Solution.Count < Number && !visited.Contains(i) && !Solution.Contains(Points[i]))
                {
                    visited.Add(i);
                    Solution.Add(Points[i]);

                    if (Solution.Count == Number)
                    {

                        double tmp = Result(Solution);
                        if (tmp < min)
                        {
                            min = tmp;
                            List.Clear();
                            List.AddRange(Solution);
                        }
                        Solution.Remove(Solution.Last());
                    }
                    else Help(visited);
                    visited.Remove(i);
                }
            }
            if (Solution.Count > 0)
            {
                Solution.Remove(Solution.Last());
                Help(visited);
            }
            
        }

    }
}
