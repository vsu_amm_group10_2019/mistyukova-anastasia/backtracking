﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace backtracking
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int n = (int)numericUpDown1.Value;
            dataGridView1.RowCount = n;
            for (int i = 0; i < n; i++)
            {
                dataGridView1.Rows[i].HeaderCell.Value = i.ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<Point> points = new List<Point>();
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                Point p = new Point();
                p.x = Convert.ToDouble(dataGridView1.Rows[i].Cells[0].Value);
                p.y = Convert.ToDouble(dataGridView1.Rows[i].Cells[1].Value);
                p.z = Convert.ToDouble(dataGridView1.Rows[i].Cells[2].Value);
                points.Add(p);
            }
            int num = (int)numericUpDown2.Value;
            string str = "";
            if (num <= dataGridView1.RowCount)
            {
                Solver solver = new Solver(num, dataGridView1.RowCount, points);
                solver.Solve();
                foreach (Point p in solver.List)
                {
                    str += $"[{p.x} ; {p.y} ; {p.z}]\n";
                }
            }
            else
            {
                str = "Нельзя найти подмножество, по размеру большее исходного множества";
            }
            MessageBox.Show(str, "Ответ");
        }
    }
}
